///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 01b - Summation
//
// Usage:  summation n
//   n:  Sum the digits from 1 to n
//
// Result:
//   The sum of the digits from 1 to n
//
// Example:
//   $ summation 6
//   The sum of the digits from 1 to 6 is 21
//
// @author Micah Chinen <micah42@hawaii.edu>
// @date   14_Jan_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
   int n = atoi(argv[1]);
   int i;
   int sum = 0;

   for(i = 1; i <= n; i++)
   {   
      sum += i;
   }
   printf("The sum of the digits from 1 to %d is %d\n", n, sum);
   return 0;
}
